#!/bin/sh


# path of wikipedia index and multistream

wiki_path=~/stuff/articles/wikipedia

tmp_dir="/tmp/wikiparse"




wikitext_to_man(){

#wiki cleanup

cat $1 |
sed 's/&lt;/</g; s/&gt;/>/g' |  # revert html escape back to <>
perl -0pe 's/{{Infobox.*?\n}}\n//gms' |
perl -0pe 's/{\| class=".*?\n\|}\n//gms' |
sed '/^|/d' |  # remove info boxes to the right of articles
sed -r "/^\{\{(内容過剰|ファンサイト的|Otheruses|Anchor)/d"  |  # remove notices on top
perl -pe 's/<ref.*?<\/ref>//gms; s/<\/*span.*?>//gms' |  # remove links, notes, etc

# manpage formatting

sed '1s/^/.TH wikipedia\n/' | # manpage title; required for proper display
sed 's/^== /.SH /g; s/ ==$//g' |  # headers
sed 's/^=== /.SS /g; s/ ===$//g' |
sed 's/^==== /.SS /g; s/ ====$//g' |

perl -pe "s/'''(.*?)'''/ \\\fI\1\\\fP /gms" |  # italicized words, using backreferences

sed 's/\[\[/ \\fB/g; s/\]\]/\\fP /g' |  # bold wikipedia article links

sed 's/^; /.SS /g; s/^: //g' |  # : and ; notation
sed 's/^:; /.TP \n /g; s/^:: //g' |  # : and ; notation

perl -pe 's/\{\{.*?\}\}//gms' # remove anything else

}

seek_article(){

dd skip=$offset count=1000000 if="$wiki_multi" of=$tmp_dir/temp.bz2 bs=1
bzip2recover $tmp_dir/temp.bz2
bunzip2 $tmp_dir/rec*temp.bz2
cat $tmp_dir/rec*temp > $tmp_dir/temp.xml
echo "<pages>" | cat - $tmp_dir/temp.xml > $tmp_dir/pages.xml
echo "</pages>" >> $tmp_dir/pages.xml
xmllint --xpath "//id[text()='${page_id}']/.." $tmp_dir/pages.xml --recover > $tmp_dir/page.xml
xmllint --xpath "//text/text()" "$tmp_dir/page.xml" > text.md

}

select_lang(){

wikilist=$(ls $wiki_path | sed 's/-.*//' | uniq)

if
[ $(echo $wikilist | wc -l) -gt 1  ]
then
	wikilang=$wikilist
else
	printf "Select wiki lang:"
	wikilang=$(printf "$wikilist" | fzf)
fi
wiki_index=$(ls "$wiki_path"/"$wikilang"*.txt*)
wiki_multi=$(ls "$wiki_path"/"$wikilang"*.xml*)

}

select_article(){

of_and_id=$(cat $wiki_index | grep "^[0-9]*:[0-9]*:$(cat $wiki_index | sed 's/^[0-9]*:[0-9]*://g' | fzf)$" | grep -o "^[0-9]*:[0-9]*:" | sed s'/.$//')

# echo $test2 | grep -o "^[0-9]*:[0-9]*:"

# of_and_id=$(echo $test2 | grep -o "^[0-9]*:[0-9]*:" | sed s'/.$//')
offset=${of_and_id%:*}
page_id=${of_and_id#*:}

}

main() {

mkdir "$tmp_dir"

select_lang || exit 1

select_article || exit 1

seek_article

GROFF_NO_SGR=1

lines=$(tput cols)
width=$((lines - 8))

wikitext_to_man text.md | preconv | groff -E -Tutf8 -rLL="$width"n -rLT=n -m man -m ja | less

}

main
rm -rf /tmp/wikiparse
