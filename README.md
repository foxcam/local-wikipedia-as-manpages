# Local Wikipedia as Manpages

A bash script that can search through a [Wikipedia multistrean database](https://dumps.wikimedia.org/) and format it as a man page. Practical for those without constant internet access and those who just like reading stuff in the terminal. Currently in a functioning state, though I want to clean it up a bit and learn more about shell script convention before I release an official version. Licensed under GPLv3, with the code on extraction taken from hoereth's [Offline Wiki Reader](https://github.com/hoereth/offline-wiki-reader) script.

Current features:

- Allows selection between multiple databases
- Convenient searching with fzf (probably not the most optimal in low-specs)
- Formatting is done without an external formatter (no need for pandoc)

Planned features:

- A better search mechanism (maybe the one in [arch-wiki-lite](http://kmkeen.com/arch-wiki-lite/))
- The ability to detect redirect articles and forward the correct one
- Proper hyphenation language detection for the rest of groff's supported languages
- Flag that searches with whatever's on the clipboard, so one can jump between linked aricles quicker
